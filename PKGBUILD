# Maintainer: Mark Wagie <mark at manjaro dot org>

pkgname=watchmate
_app_id=io.gitlab.azymohliad.WatchMate
pkgver=0.5.3
pkgrel=1
pkgdesc="PineTime smart watch companion app for Linux phone and desktop"
arch=('x86_64' 'aarch64')
url="https://github.com/azymohliad/watchmate"
license=('GPL-3.0-or-later')
depends=(
  'bluez'
  'libadwaita'
)
makedepends=(
  'cargo'
  'git'
)
source=("git+https://github.com/azymohliad/watchmate.git#tag=v$pkgver")
sha256sums=('7695e684d768a072660a5fb63efb02153519ee90501f2471fb38e02d33edec54')

prepare() {
  cd "$pkgname"
  export RUSTUP_TOOLCHAIN=stable
  cargo fetch --locked --target "$(rustc -vV | sed -n 's/host: //p')"
}

build() {
  cd "$pkgname"
  CFLAGS+=" -ffat-lto-objects"
  export RUSTUP_TOOLCHAIN=stable
  export CARGO_TARGET_DIR=target
  cargo build --frozen --release --all-features
}

check() {
  cd "$pkgname"
  appstreamcli validate --no-net "assets/${_app_id}.metainfo.xml"
  desktop-file-validate "assets/${_app_id}.desktop"
}

package() {
  cd "$pkgname"
  install -Dm755 "target/release/$pkgname" -t "$pkgdir/usr/bin/"
  install -Dm644 "assets/${_app_id}.desktop" -t "$pkgdir/usr/share/applications/"
  install -Dm644 "assets/${_app_id}.gschema.xml" -t "$pkgdir/usr/share/glib-2.0/schemas/"
  install -Dm644 "assets/${_app_id}.metainfo.xml" -t "$pkgdir/usr/share/metainfo/"
  install -Dm644 "assets/icons/${_app_id}.svg" -t \
    "$pkgdir/usr/share/icons/hicolor/scalable/apps/"
  install -Dm644 "assets/icons/${_app_id}-symbolic.svg" -t \
    "$pkgdir/usr/share/icons/hicolor/symbolic/apps/"
}
